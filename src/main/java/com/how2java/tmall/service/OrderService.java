package com.how2java.tmall.service;

import com.how2java.tmall.dao.OrderDao;
import com.how2java.tmall.pojo.Order;
import com.how2java.tmall.pojo.OrderItem;
import com.how2java.tmall.pojo.User;
import com.how2java.tmall.util.Page4Navigator;
import com.how2java.tmall.util.SpringContextUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@CacheConfig(cacheNames = "orders ")
public class OrderService {
    public static final String waitPay="waitPay";
    public static final String waitDelivery="waitDelivery";
    public static final String waitConfirm="waitConfirm";
    public static final String waitReview="waitReview";
    public static final String finish="finish";
    public static final String delete="delete";

    @Autowired
    private OrderDao orderDao;
    @Autowired
    private OrderItemService orderItemService;

    @Cacheable(key="'orders-page-'+#p0+'-'+#p1")
    public Page4Navigator<Order> list(int start,int size,int navigatePages){
        Sort sort = new Sort(Sort.Direction.DESC,"id");
        Pageable pageable = new PageRequest(start,size,sort);
        Page page = orderDao.findAll(pageable);
        return new Page4Navigator<>(page,navigatePages);
    }

    /**
     * 把订单里的订单项的订单属性设置为空
     * 为什么要做这个事情呢？ 因为SpingMVC
     * ( springboot 里内置的mvc框架是 这个东西)
     * 的 RESTFUL 注解，在把一个Order转换为json的同时，
     * 会把其对应的 orderItems 转换为 json数组，
     * 而 orderItem对象上有 order属性，
     * 这个order 属性又会被转换为 json对象，
     * 然后这个 order 下又有 orderItems 。。。
     * 就这样就会产生无穷递归，系统就会报错了
     *
     * @param orders
     */
    public void removeOrderFromOrderItem(List<Order> orders){
        for(Order order : orders){
            removeOrderFromOrderItem(order);
        }
    }

    public void removeOrderFromOrderItem(Order order){
        List<OrderItem> orderItems = order.getOrderItems();
        for(OrderItem orderItem : orderItems){
            orderItem.setOrder(null);
        }
    }



    @Cacheable(key="'orders-one-'+#p0")
    public Order get(int oid){
        return orderDao.findOne(oid);
    }

    @CacheEvict(allEntries = true)
    public void update(Order bean){
        orderDao.save(bean);
    }

    @CacheEvict(allEntries = true)
    public void add(Order bean){
        orderDao.save(bean);
    }

    //生成订单
    @Transactional(propagation= Propagation.REQUIRED,rollbackForClassName="Exception")
    public float add(Order order,List<OrderItem> ois){
        float total = 0;
        OrderService orderService = SpringContextUtil.getBean(OrderService.class);
        orderService.add(order);
        if(false){
            throw new RuntimeException();
        }

        for(OrderItem oi : ois){
            oi.setOrder(order);
            orderItemService.update(oi);
            total += oi.getProduct().getPromotePrice()*oi.getNumber();
        }
        return total;
    }

    @Cacheable(key = "'orders-'+#p0.id")
    public List<Order> listByUserWithoutDelete(User user){
        return orderDao.findByUserAndStatusNotOrderByIdDesc(user,OrderService.delete);
    }

    public void cacl(Order o){
        List<OrderItem> orderItems = o.getOrderItems();
        float total = 0;
        for(OrderItem oi : orderItems){
            total += oi.getProduct().getPromotePrice()*oi.getNumber();
        }
        o.setTotal(total);
    }


}
