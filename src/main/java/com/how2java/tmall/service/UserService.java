package com.how2java.tmall.service;


import com.how2java.tmall.dao.UserDao;
import com.how2java.tmall.pojo.User;
import com.how2java.tmall.util.Page4Navigator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserDao userDao;

    public void add(User user){
        userDao.save(user);
    }

    public Page4Navigator<User> list(int start, int size, int navigatePages){
        Sort sort = new Sort(Sort.Direction.DESC,"id");
        Pageable pageable = new PageRequest(start,size,sort);
        Page pageFromJpa = userDao.findAll(pageable);
        return new Page4Navigator<>(pageFromJpa,navigatePages);
    }

    public boolean isExist(String name){
        User user = userDao.findByName(name);
        return null!=user;
    }

    public User getUserByName(String name){
        return userDao.findByName(name);
    }

    public User getUserByNameAndPassword(String name,String password){
        return userDao.findByNameAndPassword(name,password);
    }

}
