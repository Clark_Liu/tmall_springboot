package com.how2java.tmall.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.how2java.tmall.service.OrderService;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="order_")
@JsonIgnoreProperties({"handler","hibernateLazyInitializer"})
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="orderCode")
    private String orderCode;

    @Column(name="address")
    private String address;

    @Column(name="post")
    private String post;

    @Column(name="receiver")
    private String receiver;

    @Column(name="mobile")
    private String mobile;

    @Column(name="userMessage")
    private String userMessage;

    @Column(name="createDate")
    private Date createDate;

    @Column(name="payDate")
    private Date payDate;

    @Column(name="deliveryDate")
    private Date deliveryDate;

    @Column(name="confirmDate")
    private Date confirmDate;

    @ManyToOne
    @JoinColumn(name="uid")
    private User user;

    @Column(name="status")
    private String status;

    /*---非数据库字段---*/
    //该订单瞎的订单项列表
    @Transient
    private List<OrderItem> orderItems;

    //该订单的总计金额
    @Transient
    private float total;

    //该订单的总计数量
    @Transient
    private int totalNumber;

    //状态转成中文
    @Transient
    private String statusDesc;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUserMessage() {
        return userMessage;
    }

    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Date getConfirmDate() {
        return confirmDate;
    }

    public void setConfirmDate(Date confirmDate) {
        this.confirmDate = confirmDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> ordetItems) {
        this.orderItems = ordetItems;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getTotalNumber() {
        return totalNumber;
    }

    public void setTotalNumber(int totalNumber) {
        this.totalNumber = totalNumber;
    }

    public String getStatusDesc() {
        String desc = "未知";

        switch (status){
            case OrderService.waitPay:
                desc = "待付款";
                break;
            case OrderService.waitDelivery:
                desc = "代发货";
                break;
            case OrderService.waitConfirm:
                desc = "待收货";
                break;
            case OrderService.waitReview:
                desc = "待评价";
                break;
            case OrderService.finish:
                desc = "已完成";
                break;
            case OrderService.delete:
                desc = "已删除";
                break;
            default:
                desc = "未知";
        }
        return desc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }
}
